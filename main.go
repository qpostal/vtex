package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"runtime"
	"time"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
)

var Build = "development"

func main() {
	runtime.GOMAXPROCS(1)

	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}

	appKey := os.Getenv("VTEX_API_KEY")
	appToken := os.Getenv("VTEX_API_TOKEN")

	r := chi.NewRouter()
	r.Use(middleware.Logger)
	r.Get("/", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.Write([]byte(fmt.Sprintf(`{"build": "%s"}`, Build)))
	})

	r.Get("/manifest", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.Write([]byte(`{
			  "paymentMethods": [
			    {
			      "name": "Plata",
			      "allowsSplit": "disabled"
			    }
			  ],
			  "customFields": [],
			  "autoSettleDelay": {
			    "minimum": "0",
			    "maximum": "720"
			  }
			}`))
	})

	r.Post("/callback", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.Write([]byte(`ok`))
	})

	r.Post("/payments", func(w http.ResponseWriter, r *http.Request) {
		body, err := io.ReadAll(r.Body)
		if err != nil {
			w.WriteHeader(500)
			return
		}

		var req Request
		err = json.Unmarshal(body, &req)
		if err != nil {
			w.WriteHeader(500)
			return
		}

		w.Header().Set("Content-Type", "application/json")

		w.Write([]byte(fmt.Sprintf(`{
			"paymentId": "%s",
			"tid": "TID1578324421",
			"status": "undefined"}`, req.PaymentID)))

		// w.Write([]byte(fmt.Sprintf(`{
		// 	"paymentId": "%s",
		// 	"status": "undefined",
		// 	"authorizationId": "AUT123567",
		// 	"nsu": "NSU987432",
		// 	"tid": "TID1578324421",
		// 	"acquirer": "FooBarPayments",
		// 	"delayToAutoSettle": 432000,
		// 	"delayToAutoSettleAfterAntifraud": 120,
		// 	"delayToCancel": 600}`, req.PaymentID)))

		go func() {
			time.Sleep(time.Second)

			body := []byte(fmt.Sprintf(`{
			    "paymentId": "%s",
			    "status": "approved",
				"tid": "TID1578324421",
				"authorizationId": "184520",
				"nsu": "21705348",
			    "code": "ok",
			    "message": "Successfully approved transaction"
			}`, req.PaymentID))

			// body := []byte(fmt.Sprintf(`{
			//     "paymentId": "%s",
			//     "status": "approved",
			//     "authorizationId": "184520",
			//     "nsu": "21705348",
			//     "tid": "21705348",
			//     "acquirer": "pagmm",
			//     "code": "0000",
			//     "message": "Successfully approved transaction",
			//     "delayToAutoSettle": 1200,
			//     "delayToAutoSettleAfterAntifraud": 1200,
			//     "delayToCancel": 86400,
			//     "maxValue": 16.6
			// }`, req.PaymentID))

			client := &http.Client{}
			req, err := http.NewRequest("POST", req.CallbackUrl, bytes.NewBuffer(body))
			if err != nil {
				return
			}

			req.Header.Add("Content-Type", "application/json")
			req.Header.Add("X-VTEX-API-AppKey", appKey)
			req.Header.Add("X-VTEX-API-AppToken", appToken)

			resp, err := client.Do(req)
			if err != nil {
				return
			}
			defer resp.Body.Close()
		}()
	})

	r.Post("/payments/{paymentId}/settlements", func(w http.ResponseWriter, r *http.Request) {
		body, err := io.ReadAll(r.Body)
		if err != nil {
			w.WriteHeader(500)
			return
		}

		var req Request
		err = json.Unmarshal(body, &req)
		if err != nil {
			w.WriteHeader(500)
			return
		}

		w.Header().Set("Content-Type", "application/json")

		w.Write([]byte(fmt.Sprintf(`{
			"paymentId": "%s",
			"settleId": "2EA354989E7E4BBC9F9D7B66674C2574",
			"value": 57,
			"code":"ok",
			"message": "Successfully settled",
			"requestId": "%s"}`, req.PaymentID, req.RequestID)))

		// w.Write([]byte(fmt.Sprintf(`{
		// 	"paymentId": "%s",
		// 	"settleId": "2EA354989E7E4BBC9F9D7B66674C2574",
		// 	"value": 57,
		// 	"code":"ok",
		// 	"message": "Successfully settled",
		// 	"requestId": "%s"}`, req.PaymentID, req.RequestID)))
	})

	r.Post("/payments/{paymentId}/refunds", func(w http.ResponseWriter, r *http.Request) {
		body, err := io.ReadAll(r.Body)
		if err != nil {
			w.WriteHeader(500)
			return
		}

		var req Request
		err = json.Unmarshal(body, &req)
		if err != nil {
			w.WriteHeader(500)
			return
		}

		w.Header().Set("Content-Type", "application/json")

		w.Write([]byte(fmt.Sprintf(`{
			"paymentId": "%s",
			"refundId": null,
			"value": 0,
			"code": "refund-manually",
			"message": "Successfully refunded",
			"requestId": "%s"}`, req.PaymentID, req.RequestID)))
	})

	err := http.ListenAndServe(":"+port, r)
	if err != nil {
		log.Fatal(err)
	}
}

type Request struct {
	PaymentID   string `json:"paymentId"`
	RequestID   string `json:"requestId"`
	CallbackUrl string `json:"callbackUrl"`
}
